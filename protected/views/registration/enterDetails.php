<?php
$this->breadcrumbs=array(
	'Register',
	'Enter Details',
);

$this->menu = array();

$siteKey = Yii::app()->params['recaptcha-sitekey'];
?>

<h1>Register on <?php echo Yii::app()->name; ?></h1>

<div class="form well">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'registrations-form',
	'enableAjaxValidation' => false,
)); ?>

	<p class="alert alert-info">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row-fluid">
		<?php echo $form->labelEx($model, 'givenName'); ?>
		<?php echo $form->textField($model, 'givenName', array('size' => 50, 'maxlength' => 50)); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->labelEx($model, 'sn'); ?>
		<?php echo $form->textField($model, 'sn', array('size' => 50, 'maxlength' => 50)); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->labelEx($model, 'mail'); ?>
		<?php echo $form->textField($model, 'mail', array('size' => 50, 'maxlength' => 50)); ?>
	</div>
	
	<div class="row-fluid">
                <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                <script type="text/javascript" src="https://www.recaptcha.net/recaptcha/api.js?hl=en">
                </script>
	</div>

	<div class="row-fluid buttons">
		<?php echo CHtml::submitButton('Register Account', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
