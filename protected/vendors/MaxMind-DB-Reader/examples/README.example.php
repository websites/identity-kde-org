<?php

require __DIR__ . '/../autoload.php';

use MaxMind\Db\Reader;

$ipAddress = '36.78.252.11';
$databaseFile = __DIR__ . '/../../../data/GeoLite2-City.mmdb';

$reader = new Reader($databaseFile);
print_r($reader->get($ipAddress));
$reader->close();
